# Exit unless interactive
case $- in
  *i*) ;;
  *) return;;
esac

# Path
test -d /usr/sbin && PATH=$PATH:/usr/sbin
test -d $HOME/.local/bin && PATH=$PATH:$HOME/.local/bin
test -d $HOME/.cabal/bin && PATH=$PATH:$HOME/.cabal/bin
test -d $HOME/.ghcup/bin && PATH=$PATH:$HOME/.ghcup/bin
test -d $HOME/.cargo/bin && PATH=$PATH:$HOME/.cargo/bin

# Exports
export XDG_CACHE_HOME=$HOME/.cache
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

export EDITOR=nvim
export GPG_TTY=$(tty)
export LYNX_CFG=$XDG_CONFIG_HOME/lynx/lynx.cfg
export LYNX_LSS=$XDG_CONFIG_HOME/lynx/lynx.lss
export MAILDIR=$HOME/.mail

eval $(dircolors $XDG_CONFIG_HOME/dircolors/dircolors)
eval $(ssh-agent -s) &> /dev/null

# Aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias dotfiles='git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'
alias pwstore='git --git-dir=$HOME/repos/pwstore/ --work-tree=$HOME'

# Shell and readline variables: bash(1)
HISTCONTROL=ignoreboth:erasedups
HISTFILE=$XDG_CACHE_HOME/bash_history
HISTFILESIZE=1024
HISTSIZE=1024

bind 'set colored-completion-prefix On'
bind 'set colored-stats On'
bind 'set completion-ignore-case On'
bind 'set completion-map-case On'

# Shell options: shopt(1)
shopt -s checkwinsize
shopt -s dotglob
shopt -s histappend

# Prompts
PS1="$ "
PS2="> "
PS4="+ "
