# dotfiles

My configuration files

## Install

```bash
DOTFILES_DIR=$HOME/repos/dotfiles/ # modify this to set cloning destination
test -d $DOTFILES_DIR || mkdir -p $DOTFILES_DIR
git clone --bare https://gitlab.com/vicwall/dotfiles.git $DOTFILES_DIR
git --git-dir=$DOTFILES_DIR config --local status.showUntrackedFiles no
git --git-dir=$DOTFILES_DIR --work-tree=$HOME checkout
```
