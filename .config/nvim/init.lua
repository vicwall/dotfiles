local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  vim.fn.system({ "git", "clone", "--branch=stable", lazyrepo, lazypath })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  "nvim-lua/plenary.nvim",
  "nvim-treesitter/nvim-treesitter",
  "stevearc/conform.nvim",
  { "catppuccin/nvim", name = "catppuccin", priority = 1000 },
  { "nvim-telescope/telescope.nvim", opts = {} },
}, {})

require("conform").setup({
  formatters_by_ft = {
    c = { "clang_format" },
    cpp = { "clang_format" },
    haskell = { "fourmolu" },
    lua = { "stylua" },
  },
})

require("nvim-treesitter.configs").setup({
  auto_install = true,
  highlight = { enable = true },
})

vim.cmd.colorscheme("catppuccin")
vim.keymap.set("n", "<space>g", require("telescope.builtin").live_grep)
vim.keymap.set("n", "<space>h", require("telescope.builtin").help_tags)
vim.keymap.set("n", "<space>l", require("telescope.builtin").find_files)
vim.keymap.set("n", "<space>t", require("telescope.builtin").tags)
vim.keymap.set({ "n", "v" }, "<space>f", require("conform").format)
vim.o.number = true
